import QtQuick 2.12
import QtQuick.Controls 2.12

Popup {
    id: _popup
    property bool yesClicked: false
    closePolicy: Popup.NoAutoClose
    background: Rectangle {
        color: "white"
        border {
            color: "black"
            width: 2
        }
    }

    contentItem: Item {
        Text {
            anchors.centerIn: parent
            text: "Do you really want shuffly this?"
        }

        RoundButton {
            property color buttonColor: "#c7c7c7"
            anchors.left: parent.left
            anchors.leftMargin: parent.width * 0.03
            anchors.bottom: parent.bottom
            anchors.bottomMargin: parent.height * 0.1
            width: parent.width * 0.45
            height: parent.height * 0.12
            radius: (parent.width + parent.height) / 64
            text: "Yes"
            palette {
                button: buttonColor
            }

            onPressed: {
                buttonColor = "#7f7f7f";
            }

            onReleased: {
                buttonColor = "#c7c7c7";
            }

            onClicked: {
                yesClicked = true;
                _popup.close();
            }
        }

        RoundButton {
            property color buttonColor: "#c7c7c7"
            anchors.right: parent.right
            anchors.leftMargin: parent.width * 0.03
            anchors.bottom: parent.bottom
            anchors.bottomMargin: parent.height * 0.1
            width: parent.width * 0.45
            height: parent.height * 0.12
            radius: (parent.width + parent.height) / 64
            text: "No"
            palette {
                button: buttonColor
            }

            onPressed: {
                buttonColor = "#7f7f7f";
            }

            onReleased: {
                buttonColor = "#c7c7c7";
            }

            onClicked: {
                _popup.close();
            }
        }
    }

    onClosed: {
        yesClicked = false;
    }
}
