import QtQuick 2.12

ParallelAnimation {
    id: _shakeAnimation
    property int step: 0;
    property alias firstObjectTarget: _firstAnimation.target
    property alias secondObjectTarget: _secondAnimation.target
    property int firstAnimeFromState: 0;
    property int firstAnimeToState: firstAnimeFromState + step;
    property int secondAnimeFromState: 0;
    property int secondAnimeToState: secondAnimeFromState + step;
    property int repeat: 0
    readonly property int interval: 250
    readonly property int animeDuration: 25
    readonly property int numberOfLoops: !((interval / animeDuration) % 2) ? ((interval / animeDuration) + 1) : (interval / animeDuration)
    running: false

    NumberAnimation {
        id: _firstAnimation
        from: _shakeAnimation.firstAnimeFromState
        to: _shakeAnimation.firstAnimeToState
        property: "x"
        duration: _shakeAnimation.animeDuration
    }

    NumberAnimation {
        id: _secondAnimation
        from: _shakeAnimation.secondAnimeFromState
        to: _shakeAnimation.secondAnimeToState
        property:  "x"
        duration: _shakeAnimation.animeDuration
    }

    onStopped: {
        var swap = firstAnimeToState;
        firstAnimeToState = firstAnimeFromState;
        firstAnimeFromState = swap;

        swap = secondAnimeToState;
        secondAnimeToState = secondAnimeFromState;
        secondAnimeFromState = swap;

        if (repeat < numberOfLoops) {
            start();
            ++repeat;
        } else {
            repeat = 0;
        }
    }
}
