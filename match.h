#ifndef MATCH_H
#define MATCH_H
#include <QAbstractListModel>
#include <QVector>
#include <QString>
#include <QQueue>
#include <utility>

template<typename T>
using Matrix = QVector<QVector<T>>;

class Match : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(qint32 width READ width)
    Q_PROPERTY(qint32 height READ height)

public:
    Match(QObject *parent = nullptr);
    Q_INVOKABLE bool makeMove(qint32 index1, qint32 index2);
    Q_INVOKABLE bool checkAndRemoveMathedBalls();
    Q_INVOKABLE void removeMathedBalls();
    Q_INVOKABLE void shuffle();
    qint32 width();
    qint32 height();
    int rowCount(const QModelIndex & parent = QModelIndex()) const;
    QHash<int, QByteArray> roleNames() const;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
private:
    bool m_isSwap;
    qint32 m_gameAreaWidth;
    qint32 m_gameAreaHeight;
    Matrix<qint32> m_machedRows;
    QVector<qint32> m_arrOfDeletingIndexs;
    QList <QVariant> m_colors;
    QList <QString> m_balls;
private:
    template<typename T>
    Matrix <T> init();
    void createJsonFile(QString fileName);

    void initBalls();
    QQueue<QString> randomBallColors(qint32 length);
    qint32 setCell(qint32 rowIndex, qint32 columnIndex, qint32 addToRow, qint32 addToColumn, qint32 initialValueOfMathcedBlocks, Matrix<QString> & matrixOfBalls);
    void swap(Matrix<QString> &Matrix, qint32 index1, qint32 index2);
    void findPossibleMatch(Matrix<QString> &ballsMatrix);
    void prepareIndexForDeleting(Matrix<int> matrixOfMatch);
    void shuffleBalls();
    std::pair<qint32, qint32> parseIndex(qint32 index);
    Matrix<qint32> mergeTwoMatchMatrix(Matrix<qint32> rowMatchMatrix, Matrix<qint32> columnsMatchMatrix);
    Matrix<QString> copy(const QList<QString> items);
    QList<QString> copy(Matrix<QString> items);
    void getDataFromJson();
};

template<typename T>
Matrix <T> Match::init() {
    Matrix <T> initialMatrix(m_gameAreaHeight);
    std::for_each(initialMatrix.begin(), initialMatrix.end(), [&](QVector<T> & vec) { vec = QVector<T>(m_gameAreaWidth); } );
    return initialMatrix;
}

#endif // MATCH_H
