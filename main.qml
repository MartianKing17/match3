import QtQuick 2.10
import QtQuick.Window 2.10
import QtQuick.Controls 2.12
import match 1.0
import QtGraphicalEffects 1.0

Window {
    visible: true
    width: 640
    height: 800
    title: qsTr("Match3")

    OverGameMenu {
        id: _popup
        anchors.centerIn: parent
        visible: false
        width: parent.width / 2
        height: parent.height / 3
        z: 2
    }

    Rectangle {
        id: _mask
        anchors.centerIn: parent
        visible: _popup.opened
        width: parent.width
        height: parent.height
        z: 1
        color: "transparent"

        GaussianBlur {
            anchors.fill: _mask
            source: _area
            deviation: 4
            radius: 8
            samples: 16
        }
    }

    Rectangle {
        id: _area
        property bool isAreaClickable: !_popup.opened
        anchors.fill: parent

        GameBoard {
            id: _gameBoard
            width: parent.width
            height: parent.height * 0.8
            isClickable: parent.isAreaClickable
            shuffle: _popup.yesClicked
        }

        MatchButton {
            id: _shuffle
            anchors.top: _gameBoard.bottom
            anchors.topMargin: height * 0.1
            width: parent.width
            height: parent.height - _gameBoard.height
            color: "#c7c7c7"
            text: "Shuffle"
            isClickable: parent.isAreaClickable
            onClicked: {
                _popup.open();
            }
            onPressed: {
                color = "#7f7f7f";
            }
            onReleased: {
                color = "#c7c7c7";
            }
        }

        onIsAreaClickableChanged: {
            console.log("Area clickable: " + isAreaClickable)
        }
    }
}
