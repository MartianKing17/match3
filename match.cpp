#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QFile>
#include <algorithm>
#include <random>
#include "match.h"

#include <iostream>

Match::Match(QObject *parent) :
    QAbstractListModel(parent),
    m_isSwap(false),
    m_gameAreaWidth(4),
    m_gameAreaHeight(4),
    m_colors{"#FF0000", "#FF0000", "#FF0000","#FF0000"}
{
    getDataFromJson();
    m_machedRows = init<qint32>();
    initBalls();
}

void Match::createJsonFile(QString fileName) {
    QFile jsonFile(fileName);
    QJsonObject jsonObject;
    QJsonArray colorArr;
    std::for_each(m_colors.begin(), m_colors.end(), [&](const QVariant &color) { colorArr.append(color.toString());});
    jsonObject.insert("width", m_gameAreaWidth);
    jsonObject.insert("height", m_gameAreaHeight);
    jsonObject.insert("color", colorArr);

    if (!jsonFile.open(QIODevice::WriteOnly | QFile::Text)) {
        return;
    }

    QJsonDocument doc(jsonObject);
    jsonFile.write(doc.toJson());
    jsonFile.close();
}

void Match::getDataFromJson()
{
    const QString fileName = "setting.json";
    QFile jsonFile;
    QJsonObject jsonData;
    jsonFile.setFileName(fileName);
    qint32 width = 0, height = 0;
    QList <QVariant> colors;

    if (!jsonFile.exists()) {
        createJsonFile(fileName);
    }

    if (!jsonFile.open(QIODevice::ReadOnly | QIODevice::Text)) {
        return;
    }

    jsonData = QJsonDocument::fromJson(jsonFile.readAll()).object();
    jsonFile.close();
    width = jsonData["width"].toInt();
    height = jsonData["height"].toInt();
    colors = jsonData["color"].toArray().toVariantList();

    if (width > 0) {
        m_gameAreaWidth = width;
    }

    if (height > 0) {
        m_gameAreaHeight = height;
    }

    if (!colors.empty()) {
        m_colors = colors;
    }
}

int Match::rowCount(const QModelIndex & parent) const
{
    Q_UNUSED(parent)
    return m_balls.size();
}

QHash<int, QByteArray> Match::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles.insert(0, "color");
    return roles;
}

QVariant Match::data(const QModelIndex &index, int role) const
{
    Q_UNUSED(role)
    if (!index.isValid()) {
        QVariant();
    }

    qint32 row = index.row();

    if (row < m_balls.size()) {
        return QVariant(m_balls.at(row));
    }
    return QVariant();
}

void Match::shuffleBalls() {
    Matrix<QString> matrixOfBallColors;

    do {
        std::random_shuffle(m_balls.begin(), m_balls.end());
        m_arrOfDeletingIndexs.clear();
        matrixOfBallColors = copy(m_balls);
        findPossibleMatch(matrixOfBallColors);
    } while (!m_arrOfDeletingIndexs.empty());
}

QList <QString> Match::copy(Matrix<QString> matrix)
{
    QList <QString> dublicate;

    for (QVector<QString> vec : matrix) {
        std::for_each(vec.begin(), vec.end(), [&](const QString color) { dublicate.append(color); });
    }

    return dublicate;
}

Matrix<QString> Match::copy(const QList<QString> items)
{
    Matrix<QString> copyMatrix = init<QString>();
    QVector <QString> buffer;
    qint32 row = 0;

    for (qint32 i = 0; i < items.size(); ++i) {

        if (!(i  % m_gameAreaWidth) && i != 0) {
            copyMatrix[row] = buffer;
            buffer.clear();
            ++row;
        }
        buffer.push_back(items.at(i));
    }

    if (!buffer.empty()) {
        copyMatrix[row] = buffer;
    }

    return copyMatrix;
}

std::pair<qint32, qint32> Match::parseIndex(qint32 index)
{
    std::pair<qint32, qint32> newIndex;
    qint32 lvalue = 0;
    qint32 rvalue = m_gameAreaWidth;
    qint32 step = m_gameAreaWidth;

    for (qint32 row = 0; row < m_gameAreaHeight; ++row) {

        if (index >= lvalue && index < rvalue) {
            newIndex.first = index - lvalue;
            newIndex.second = row;
        }

        lvalue += step;
        rvalue += step;
    }

    return newIndex;
}

void Match::initBalls() {
    qint32 iterIndexOfColors = 0, iter = 0, length = m_gameAreaWidth * m_gameAreaHeight;


    // Fill m_balls some colors
    while (iter < length) {

        if (iterIndexOfColors >= m_colors.size()) {
            iterIndexOfColors = 0;
        }

        m_balls.append(m_colors.at(iterIndexOfColors).toString());
        ++iterIndexOfColors;
        ++iter;
    }
    shuffleBalls();
}

QQueue<QString> Match::randomBallColors(qint32 length) {
        QQueue<QString> randomColorForNewBalls;
        std::random_device rd;
        std::mt19937_64  gen(rd());
        std::uniform_int_distribution<int> dis(0, m_colors.size() - 1);

        for (qint32 i = 0; i < length; ++i) {
            randomColorForNewBalls.enqueue(m_colors.at(dis(gen)).toString());
        }

        return randomColorForNewBalls;

}

void Match::swap(Matrix<QString> & matrix, qint32 index1, qint32 index2)
{
    std::pair<qint32, qint32> parsedIndex1 = parseIndex(index1);
    std::pair<qint32, qint32> parsedIndex2 = parseIndex(index2);
    QString swappingValue = matrix[parsedIndex1.second][parsedIndex1.first];
    matrix[parsedIndex1.second][parsedIndex1.first] = matrix[parsedIndex2.second][parsedIndex2.first];
    matrix[parsedIndex2.second][parsedIndex2.first] = swappingValue;
}

void Match::findPossibleMatch(Matrix<QString> &ballsMatrix) {
    Matrix<qint32> rowMatrixOfMatch = init<qint32>();
    Matrix<qint32> columnMatrixOfMatch = init<qint32>();

    for (qint32 rowIndex = 0; rowIndex < m_gameAreaHeight; ++rowIndex){
        setCell(rowIndex, 0, 0, 1, 1, ballsMatrix);
    }

    columnMatrixOfMatch = m_machedRows;

    for (qint32 columnIndex = 0; columnIndex < m_gameAreaWidth; ++columnIndex) {
        setCell(0, columnIndex, 1, 0, 1, ballsMatrix);
    }

    rowMatrixOfMatch = m_machedRows;
    Matrix<qint32> mergedMatrix = mergeTwoMatchMatrix(rowMatrixOfMatch, columnMatrixOfMatch);
    prepareIndexForDeleting(mergedMatrix);
}

bool Match::makeMove(qint32 index1, qint32 index2)
{
    bool result{};
    qint32 horizontal = index1 < index2 ? 1 : 0;
    qint32 vertical = index1 < index2 ? 0 : 1;
    Matrix<QString> ballsMatrix = copy(m_balls);
    swap(ballsMatrix, index1, index2);
    findPossibleMatch(ballsMatrix);

    if (m_arrOfDeletingIndexs.empty()) {
        return false;
    }

    bool first = std::find(m_arrOfDeletingIndexs.begin(), m_arrOfDeletingIndexs.end(), index1) != m_arrOfDeletingIndexs.end();
    bool second = std::find(m_arrOfDeletingIndexs.begin(), m_arrOfDeletingIndexs.end(), index2) != m_arrOfDeletingIndexs.end();

    if (first || second) {

        if (std::abs(index1 - index2) == 1) {
            beginMoveRows(QModelIndex(), index1, index1, QModelIndex(), index2 + horizontal);
            endMoveRows();
        } else if (std::abs(index1 - index2) == int(m_gameAreaWidth)){
            beginMoveRows(QModelIndex(), index1, index1, QModelIndex(), index2);
            endMoveRows();
            beginMoveRows(QModelIndex(), index2 + vertical, index2 + vertical, QModelIndex(), index1 + vertical);
            endMoveRows();
        }
        result = true;
    } else {
        m_arrOfDeletingIndexs.clear();
        result = false;
    }

    m_balls = copy(ballsMatrix);
    return result;
}

qint32 Match::width()
{
    return m_gameAreaWidth;
}

qint32 Match::height()
{
    return m_gameAreaHeight;
}

qint32 Match::setCell(qint32 rowIndex, qint32 columnIndex, qint32 addToRow, qint32 addToColumn, qint32 initialValueOfMathcedBlocks, Matrix<QString> & matrixOfBalls) {
    qint32 returnInitValue{};
    bool isCanSwap{};
    bool isSwapPrev{};

    if (columnIndex + addToColumn >= m_gameAreaWidth || rowIndex + addToRow >= m_gameAreaHeight) {
        m_machedRows[rowIndex][columnIndex] = initialValueOfMathcedBlocks;
        return initialValueOfMathcedBlocks;
    }

    if (matrixOfBalls[rowIndex][columnIndex] == matrixOfBalls[rowIndex + addToRow][columnIndex + addToColumn]) {
        isCanSwap = true;
        isSwapPrev = m_isSwap;
        m_isSwap = isCanSwap;
        returnInitValue = setCell(rowIndex + addToRow, columnIndex + addToColumn, addToRow, addToColumn, initialValueOfMathcedBlocks + 1, matrixOfBalls);
    } else {
        isSwapPrev = m_isSwap;
        m_isSwap = isCanSwap;
        returnInitValue = setCell(rowIndex + addToRow, columnIndex + addToColumn, addToRow, addToColumn, 1, matrixOfBalls);
    }

    // If in the previous calling, color ball equal to the next color ball, m_isSwap set to true, else set to false.
    if (m_isSwap) {
        m_machedRows[rowIndex][columnIndex] = returnInitValue;
    } else {
        m_machedRows[rowIndex][columnIndex] = initialValueOfMathcedBlocks;
    }

    m_isSwap = isSwapPrev;
    return m_machedRows[rowIndex][columnIndex];
}

void Match::prepareIndexForDeleting(Matrix<qint32> matrixOfMatch)
{
    const qint32 limitValueForMatches = 3;
    for (qint32 i = 0; i < m_gameAreaHeight; ++i) {
        for (qint32 j = 0; j < m_gameAreaWidth; ++j) {
            if (matrixOfMatch[i][j] >= limitValueForMatches) {
                m_arrOfDeletingIndexs.push_back(i * m_gameAreaWidth + j);
            }
        }
    }
}

Matrix<qint32> Match::mergeTwoMatchMatrix(Matrix<qint32> rowMatchMatrix, Matrix<qint32> columnsMatchMatrix)
{
    QVector<qint32> buffer;
    Matrix<qint32> resultMatrix = init<qint32>();

    for (qint32 row = 0; row < m_gameAreaHeight; ++row) {
        for (qint32 column = 0; column < m_gameAreaWidth; ++column) {
            if (rowMatchMatrix[row][column] < columnsMatchMatrix[row][column]) {
                buffer.push_back(columnsMatchMatrix[row][column]);
            } else {
                buffer.push_back(rowMatchMatrix[row][column]);
            }
        }
        resultMatrix[row] = buffer;
        buffer.clear();
    }

    rowMatchMatrix.clear();
    columnsMatchMatrix.clear();
    return resultMatrix;
}

void Match::removeMathedBalls()
{
    QQueue<QString> colorsForNewBalls = randomBallColors(m_arrOfDeletingIndexs.size());
    QString color = "";

    for (const qint32 ind : m_arrOfDeletingIndexs) {
        beginRemoveRows(QModelIndex(), ind, ind);
        m_balls.removeAt(ind);
        endRemoveRows();
        beginInsertRows(QModelIndex(), ind,ind);
        m_balls.insert(ind, "white");
        endInsertRows();
    }

    for (const qint32 ind : m_arrOfDeletingIndexs) {
        for (int index = ind; index > -1; index -= m_gameAreaWidth) {
            if (index > m_gameAreaWidth - 1) {
                beginMoveRows(QModelIndex(), index - m_gameAreaWidth, index - m_gameAreaWidth, QModelIndex(), index);
                endMoveRows();
                beginMoveRows(QModelIndex(), index, index, QModelIndex(), index - m_gameAreaWidth);
                m_balls.swapItemsAt(index - m_gameAreaWidth, index);
                endMoveRows();
            } else {
                beginRemoveRows(QModelIndex(), index, index);
                m_balls.removeAt(index);
                endRemoveRows();
                beginInsertRows(QModelIndex(), index, index);
                m_balls.insert(index, colorsForNewBalls.dequeue());
                endInsertRows();
            }
        }
    }

    m_arrOfDeletingIndexs.clear();
}

bool Match::checkAndRemoveMathedBalls()
{
    bool isSuccess{};
    Matrix<QString> ballsMatrix = copy(m_balls);
    findPossibleMatch(ballsMatrix);
    if (!m_arrOfDeletingIndexs.empty()) {
        removeMathedBalls();
        isSuccess = true;
    }
    return isSuccess;
}

void Match::shuffle()
{
    beginResetModel();
    shuffleBalls();
    endResetModel();
}

