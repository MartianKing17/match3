import QtQuick 2.12
import match 1.0

Rectangle {
    property bool shuffle: false
    property bool isClickable: true

    /*

      * Bug with clickable state. Maybe because it's changes when start animation

      */

    onIsClickableChanged: {
        console.log("Clickable status: " + isClickable)
    }

    ShakeAnimation {
        id: _shake
    }

    GridView {
        id: _view
        signal trigger;
        property bool isFirstButtonSet: false
        property bool isClickable: parent.isClickable
        readonly property int row: model.height
        readonly property int column: model.width
        readonly property int animetionDuration: 2000
        property Ball firstBall;
        property Ball secondBall;
        anchors.fill: parent
        interactive: false
        model: Match { }
        cellWidth: width / column
        cellHeight: height / row

        onIsClickableChanged: {
            console.log("Clickable gridview status: " + isClickable)
        }

        move: Transition {
            NumberAnimation {
                properties: "x,y"
                duration: _view.animetionDuration / 2
                easing.type: Easing.OutBounce
            }

            onRunningChanged: {
                if (!running) {
                    _view.model.removeMathedBalls();
                    _view.isClickable = false;
                }
            }
        }

        add: Transition {
            NumberAnimation {
                property: "y"
                from: -250
                duration: _view.animetionDuration
                easing.type: Easing.OutBounce
            }

            onRunningChanged: {
                if (!running) {
                    _view.model.checkAndRemoveMathedBalls();
                    _view.isClickable = true;
                }
            }
        }

        remove: Transition {
            NumberAnimation {
                property: "scale"
                from: 1
                to: 0
                duration: _view.animetionDuration / 4
                easing.type: Easing.OutBounce
            }

            onRunningChanged: {
                if (!running) {
                    _view.model.checkAndRemoveMathedBalls();
                    _view.isClickable = true;
                }
            }
        }

        delegate: Ball {
            width: _view.cellWidth <= _view.cellHeight ? _view.cellWidth : _view.cellHeight
            height: _view.cellWidth <= _view.cellHeight ? _view.cellWidth : _view.cellHeight
            isClickable: _view.isClickable
            color: model.color
            onClicked: {
                if (!_view.isFirstButtonSet) {
                    this.indexF = index;
                    _view.firstBall = this;
                    _view.isFirstButtonSet = true;
                    color = Qt.darker(color);
                }

                if (this.indexF === index) {
                    return;
                }

                this.indexF = index;
                _view.secondBall = this;
                _view.trigger();
            }
        }

        onTrigger: {
            if (!_view.model.makeMove(firstBall.indexF, secondBall.indexF)) {
                var step = parent.width * 0.05;
                _shake.firstObjectTarget = firstBall;
                _shake.secondObjectTarget = secondBall;
                _shake.firstAnimeFromState = firstBall.x;
                _shake.firstAnimeToState = firstBall.x + step;
                _shake.secondAnimeFromState = secondBall.x;
                _shake.secondAnimeToState = secondBall.x + step;
                _shake.start();
                firstBall.indexF = -1;
                secondBall.indexF = -1;
            }
            firstBall.color = _internal.lighter(firstBall.color);
            isFirstButtonSet = false;
        }
    }

    onShuffleChanged: {
        if (shuffle) {
            _view.model.shuffle();
        }
    }

    QtObject {
        id: _internal

        function lighter(color) {
            return Qt.rgba(color.r * 2, color.g * 2, color.b * 2, color.a);
        }
    }
}
