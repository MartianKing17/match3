import QtQuick 2.12

Rectangle {
    signal clicked();
    signal pressed();
    signal released();
    property alias text: _txt.text
    property bool isClickable: true
    color: "transparent"

    Text {
        id: _txt
        anchors.centerIn: parent
    }

    MouseArea {
        anchors.fill: parent
        enabled: parent.isClickable
        onClicked: {
            parent.clicked();
        }
        onPressed: {
            parent.pressed();
        }
        onReleased: {
            parent.released();
        }
    }
}
